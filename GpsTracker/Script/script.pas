﻿var Web : TWebBrowser;

procedure Form1_OnShow (Sender: string; Action: string);
begin
    Web.Width := Form1.Panel1.Width;
    Web.Height := Form1.Panel1.Height;
end;

function toDecimalDegrees(coordinates : Extended) : Extended;
var
    b : integer; //to store the degrees
begin
   b := Int(coordinates/100); // 51 degrees

   result := (coordinates/100 - b)*100 ; //(51.536605 - 51)* 100 = 53.6605
   result := result / 60; // 53.6605 / 60 = 0.8943417
   result := result + b; // 0.8943417 + 51 = 51.8943417
end;


procedure Form1_Button1_OnClick (Sender: string; var Cancel: boolean);
var
   sl, sl1 : TStringList;
   arrStr: array of string;
   i : integer;
   line : string;
   code : string;
   html : string;

   lat, lon : Extended;
                                                 
   degrees : integer;
   minutes : integer;
   seconds : Extended;

   tempFile : string;

begin

    tempFile := 'C:\Windows\Temp\osm.html';

    DeleteFile('csvData.txt');
    HTTPGetFile('http://192.168.4.1', 'csvData.txt');

    sl := TStringList.Create;
    sl.LoadFromFile('csvData.txt');


    code := 'var points = [';

    i := 0;

    while i < sl.Count - 1 do
    begin
       line := sl[i];
       i := i + 1;
       arrStr := SplitString(line, ',');

       lat := 0.0;
       lon := 0.0;

       if arrStr[2] = 'A' then
       begin
           arrStr[3] := ReplaceStr(arrStr[3],'.',',');
           arrStr[5] := ReplaceStr(arrStr[5],'.',',');

           lat := StrToFloat(arrStr[3]);
           lon := StrToFloat(arrStr[5]);

           lat := toDecimalDegrees(lat);
           lon := toDecimalDegrees(lon);

           code := code + 'new OpenLayers.Geometry.Point(' + ReplaceStr(FloatToStr(lon),',','.') + ', ' + ReplaceStr(FloatToStr(lat),',','.') + ').transform(epsg4326, projectTo),' + #13#10;
        end;
    end;

    code := code + '];' + #13#10;


    code := code + 'var lonLat = new OpenLayers.LonLat(' + ReplaceStr(FloatToStr(lon),',','.') + ',' +  ReplaceStr(FloatToStr(lat),',','.') + ').transform(epsg4326, projectTo); var zoom=16; map.setCenter (lonLat, zoom);';

    sl := TStringList.Create;
    sl.LoadFromFile('osm.html');

    DeleteFile(tempFile);

    i := 0;
    while i < sl.Count - 1 do
    begin
       line := sl[i];
       i := i + 1;

       line := ReplaceStr(line, '${DATA}', code);

       WriteLnToFile(tempFile, line);
    end;
    
    Web.Navigate(tempFile);
end;


begin

    Web := TWebBrowser.Create(Form1.Panel1);
    TWinControl(Web).Parent := Form1.Panel1;

    Web.Width := Form1.Panel1.Width;
    Web.Height := Form1.Panel1.Height;

end.
