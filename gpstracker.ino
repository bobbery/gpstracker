#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <SPI.h>
#include <SD.h>

const char *ssid = "HOS-RV1";
const char *password = "Hennesy1";

ESP8266WebServer server(80);
 
void handleRoot() 
{
  File entry =  SD.open("datalog.txt", FILE_READ);

  if (entry)
  {
    server.streamFile(entry, "text/plain");
    entry.close();
  }
  else
  {
    server.send(200, "text/html", "Keine Daten !!");
  }
}


void deleteSd() 
{
   SD.remove("datalog.txt");
   server.send(200, "text/html", "Sauber !!");
}


void setup() 
{
	delay(1000);
	Serial.begin(9600);

	WiFi.mode(WIFI_AP);
	WiFi.softAP(ssid, password);

  Serial.print("Initialisiere SD-Karte...");

  if (!SD.begin(4)) 
  {
    Serial.println(" Fehler");
    return;
  }
  Serial.println(" OK");

	IPAddress myIP = WiFi.softAPIP();

	server.on("/", handleRoot);	
  server.on("/delete", deleteSd);  
	
	server.begin();

 
	Serial.println("HTTP Server gestartet");
  
}

enum Tokens
{
  MARKER,UTC,GPS_STATUS,LAT,NORS,LON,EORW,SPEED,COURSE,DATE
};


double deg2rad(double deg) 
{
  return (deg * PI / 180);
}

double distance(double lat1, double lon1, double lat2, double lon2) 
{
  double R = 6371;
  double dLat = deg2rad(lat2-lat1);
  double dLon = deg2rad(lon2-lon1);
  double a = sin(dLat/2) * sin(dLat/2) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon/2) * sin(dLon/2);
  double c = 2 * atan2(sqrt(a), sqrt(1-a));
  double d = R * c;
  return fabs(d);
}

double toDecimalDegrees(double coordinates)
{ 
   int b;//to store the degrees
   double c; //to store de decimal
 
   b = coordinates/100; // 51 degrees
   c= (coordinates/100 - b)*100 ; //(51.536605 - 51)* 100 = 53.6605
   c /= 60; // 53.6605 / 60 = 0.8943417
   c += b; // 0.8943417 + 51 = 51.8943417
   return c;
}

double oldLat = 0.0;
double oldLon = 0.0;


void loop() 
{
  if (Serial.available())
  {
      char buf[1024];
      int byteCount = Serial.readBytesUntil('\r', buf, 1024);
      buf[byteCount] = 0;
      String s = buf;
      
      s.trim();
      if (s.startsWith("$GPRMC"))    
      {
        String tokens[20];
        uint8_t i = 0;
        char * pch = strtok (buf,",");
        while (pch != NULL)
        {
          tokens[i++] = pch;
          pch = strtok (NULL, ",");
        }

        if (tokens[GPS_STATUS] == "A")
        {
          double latWgs = toDecimalDegrees(tokens[LAT].toFloat());
          double lonWgs = toDecimalDegrees(tokens[LON].toFloat());

          if (tokens[NORS] == "S")
          {
            latWgs *= -1.0;
          }
          if (tokens[EORW] == "W")
          {
            lonWgs *= -1.0;
          }

          double dist = distance(oldLat, oldLon, latWgs, lonWgs);

          Serial.println(dist,6);

          if (dist > 0.1)
          {
             oldLat = latWgs;
             oldLon = lonWgs;
            
             File dataFile = SD.open("datalog.txt", FILE_WRITE);
             // if the file is available, write to it:
             if (dataFile) 
             {
                dataFile.println(s);
                dataFile.close();
             }
          }
        }
      }
  }
  
	server.handleClient();
}




